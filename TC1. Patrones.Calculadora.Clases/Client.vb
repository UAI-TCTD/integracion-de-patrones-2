﻿Public Class Client
    Dim context As Context
    Dim lista As List(Of Expression)

    Dim expressions As Stack


    Public Sub New()
        context = New Context
        lista = New List(Of Expression)
    End Sub


    Private Function parser(op As String) As IList
        Dim lst As New List(Of String)
        Dim tmp As String = vbNullString

        For Each e In op.ToCharArray
            If e = "+" Or e = "-" Or e = "*" Or e = "/" Then
                'si es una operacion
                lst.Add(tmp)
                lst.Add(e)
                tmp = vbNullString
            Else
                'concateno
                tmp &= e
            End If
        Next

        lst.Add(tmp)
        Return lst
    End Function


    Private Sub Componer(operacion As String, exp As Expression)

        Dim e As Expression
        Dim lista As List(Of String) = parser(operacion)


        For Each ex In lista
            Dim operador = context.ObtenerOperador(ex)



            If operador >= 0 Then
                e = New OperatorTerminalExpression(operador)

            Else
                Dim extmp As String = ex.ToString
                If extmp.Length = 1 Then
                    'es una operacion
                    e = New OperationTerminalExpression(ex)
                ElseIf extmp.Length > 1 Then
                    'es un termino
                    e = New TerminoNonTerminalExpression()
                    Componer(ex, e)


                End If
            End If
            exp.AddExpression(e)
        Next

    End Sub


    Public Function Calcular(operacion As String) As Double 'entrada del cliente







        lista = New List(Of Expression)
        expressions = New Stack
        Dim tmp As New ArrayList

        'separo en terminos de forma sencilla (solo usando mas y menos)
        Dim expresion_sumas() As String
        expresion_sumas = operacion.Split("+")

        For Each ex As String In expresion_sumas
            tmp.Add(ex)
            tmp.Add("+")
        Next

        tmp.RemoveAt(tmp.Count - 1)




        For Each ex As String In tmp

            For Each e As String In ex.Split("-")
                expressions.Push(e)
                expressions.Push("-")
            Next
            expressions.Pop() 'saco el menos que sobra

        Next

        'armo el arbol semántico
        Dim exp As Expression
        For Each ex As String In expressions
            Dim operador = context.ObtenerOperador(ex)
            If operador >= 0 Then
                exp = New OperatorTerminalExpression(operador)

            Else
                If ex.Length = 1 Then
                    'es una operacion
                    exp = New OperationTerminalExpression(ex)
                ElseIf ex.Length > 1 Then
                    'es un termino
                    exp = New TerminoNonTerminalExpression()
                    Componer(ex, exp)

                End If
            End If

            lista.Add(exp)
        Next



        'limpio el resultado anterior
        context.Limpiar()
        'calculo el resultado
        For Each l In lista
            l.Interpret(context)
        Next



        Return context.Resultado
    End Function


End Class
