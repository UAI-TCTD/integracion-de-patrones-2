﻿Public Class TerminoNonTerminalExpression
    Inherits Expression


    Public list As New List(Of Expression)


    Public Overrides Sub Interpret(Context As Context)


        Dim ctx As New Context

        For Each e In list
            e.Interpret(ctx)
        Next

        Context.Calcular(ctx.Resultado)

    End Sub

    Public Overrides Sub AddExpression(ex As Expression)
        list.Add(ex)
    End Sub
End Class
