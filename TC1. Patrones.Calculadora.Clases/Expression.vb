﻿
Public MustInherit Class Expression

    MustOverride Sub Interpret(Context As Context)

    MustOverride Sub AddExpression(ex As Expression)

End Class
