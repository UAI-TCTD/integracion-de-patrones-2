﻿Imports TC1.Patrones.Calculadora.Clases

Public Class frmCalculadora
    'memento originator
    Dim operacion As String
    Dim calculadora As New Client

    Dim _resultado As Long = 0
    Dim _caretaker As New Caretaker


    Private Sub agregar(InputValue As String)


        If operacion = "0" Then operacion = String.Empty
        operacion += InputValue
        Me.lblResultado.Text = operacion
    End Sub

    Private Sub cmd1_Click(sender As Object, e As EventArgs) Handles cmd1.Click
        agregar(1)
    End Sub

    Private Sub cmd2_Click(sender As Object, e As EventArgs) Handles cmd2.Click
        agregar(2)
    End Sub

    Private Sub cmd3_Click(sender As Object, e As EventArgs) Handles cmd3.Click
        agregar(3)
    End Sub

    Private Sub cmdSuma_Click(sender As Object, e As EventArgs) Handles cmdSuma.Click
        agregar("+")
    End Sub

    Private Sub cmd4_Click(sender As Object, e As EventArgs) Handles cmd4.Click
        agregar(4)
    End Sub

    Private Sub cmd5_Click(sender As Object, e As EventArgs) Handles cmd5.Click
        agregar(5)
    End Sub

    Private Sub cmd6_Click(sender As Object, e As EventArgs) Handles cmd6.Click
        agregar(6)
    End Sub

    Private Sub cmd7_Click(sender As Object, e As EventArgs) Handles cmd7.Click
        agregar(7)
    End Sub

    Private Sub cmd8_Click(sender As Object, e As EventArgs) Handles cmd8.Click
        agregar(8)
    End Sub

    Private Sub cmd9_Click(sender As Object, e As EventArgs) Handles cmd9.Click
        agregar(9)
    End Sub

    Private Sub cmd0_Click(sender As Object, e As EventArgs) Handles cmd0.Click
        agregar(0)
    End Sub

    Private Sub cmdM_Click(sender As Object, e As EventArgs) Handles cmdM.Click
        'save to memento
        _caretaker.CreateMemento(New Memento(_resultado))
    End Sub

    Private Sub cmdDivision_Click(sender As Object, e As EventArgs) Handles cmdDivision.Click
        agregar("/")
    End Sub

    Private Sub cmdResta_Click(sender As Object, e As EventArgs) Handles cmdResta.Click
        agregar("-")
    End Sub

    Private Sub cmdMultiplicacion_Click(sender As Object, e As EventArgs) Handles cmdMultiplicacion.Click
        agregar("*")
    End Sub

    Private Sub cmdIgual_Click(sender As Object, e As EventArgs) Handles cmdIgual.Click
        'calcular
        Dim res As Long
        res = calculadora.Calcular(operacion)
        Me.lblResultado.Text = res
        Me.operacion = res
        Me._resultado = res

    End Sub

    Private Sub cmdC_Click(sender As Object, e As EventArgs) Handles cmdC.Click
        Me.lblResultado.Text = 0
        Me._resultado = 0
        Me.operacion = 0
    End Sub

    Private Sub cmdMR_Click(sender As Object, e As EventArgs) Handles cmdMR.Click
        _resultado = _caretaker.GetMemento().Resultado
        Me.lblResultado.Text = _resultado
    End Sub
End Class
