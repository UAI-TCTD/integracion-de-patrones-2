﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCalculadora
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.cmd1 = New System.Windows.Forms.Button()
        Me.cmd2 = New System.Windows.Forms.Button()
        Me.cmd3 = New System.Windows.Forms.Button()
        Me.cmd4 = New System.Windows.Forms.Button()
        Me.cmd5 = New System.Windows.Forms.Button()
        Me.cmd6 = New System.Windows.Forms.Button()
        Me.cmd7 = New System.Windows.Forms.Button()
        Me.cmd8 = New System.Windows.Forms.Button()
        Me.cmd9 = New System.Windows.Forms.Button()
        Me.cmdResta = New System.Windows.Forms.Button()
        Me.cmdMultiplicacion = New System.Windows.Forms.Button()
        Me.cmdDivision = New System.Windows.Forms.Button()
        Me.cmdM = New System.Windows.Forms.Button()
        Me.cmdMR = New System.Windows.Forms.Button()
        Me.cmdSuma = New System.Windows.Forms.Button()
        Me.lblResultado = New System.Windows.Forms.Label()
        Me.cmd0 = New System.Windows.Forms.Button()
        Me.cmdIgual = New System.Windows.Forms.Button()
        Me.cmdC = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'cmd1
        '
        Me.cmd1.Location = New System.Drawing.Point(12, 48)
        Me.cmd1.Name = "cmd1"
        Me.cmd1.Size = New System.Drawing.Size(47, 45)
        Me.cmd1.TabIndex = 0
        Me.cmd1.Text = "1"
        Me.cmd1.UseVisualStyleBackColor = True
        '
        'cmd2
        '
        Me.cmd2.Location = New System.Drawing.Point(65, 48)
        Me.cmd2.Name = "cmd2"
        Me.cmd2.Size = New System.Drawing.Size(47, 45)
        Me.cmd2.TabIndex = 1
        Me.cmd2.Text = "2"
        Me.cmd2.UseVisualStyleBackColor = True
        '
        'cmd3
        '
        Me.cmd3.Location = New System.Drawing.Point(118, 48)
        Me.cmd3.Name = "cmd3"
        Me.cmd3.Size = New System.Drawing.Size(47, 45)
        Me.cmd3.TabIndex = 2
        Me.cmd3.Text = "3"
        Me.cmd3.UseVisualStyleBackColor = True
        '
        'cmd4
        '
        Me.cmd4.Location = New System.Drawing.Point(12, 99)
        Me.cmd4.Name = "cmd4"
        Me.cmd4.Size = New System.Drawing.Size(47, 45)
        Me.cmd4.TabIndex = 3
        Me.cmd4.Text = "4"
        Me.cmd4.UseVisualStyleBackColor = True
        '
        'cmd5
        '
        Me.cmd5.Location = New System.Drawing.Point(65, 99)
        Me.cmd5.Name = "cmd5"
        Me.cmd5.Size = New System.Drawing.Size(47, 45)
        Me.cmd5.TabIndex = 4
        Me.cmd5.Text = "5"
        Me.cmd5.UseVisualStyleBackColor = True
        '
        'cmd6
        '
        Me.cmd6.Location = New System.Drawing.Point(118, 99)
        Me.cmd6.Name = "cmd6"
        Me.cmd6.Size = New System.Drawing.Size(47, 45)
        Me.cmd6.TabIndex = 5
        Me.cmd6.Text = "6"
        Me.cmd6.UseVisualStyleBackColor = True
        '
        'cmd7
        '
        Me.cmd7.Location = New System.Drawing.Point(12, 150)
        Me.cmd7.Name = "cmd7"
        Me.cmd7.Size = New System.Drawing.Size(47, 45)
        Me.cmd7.TabIndex = 6
        Me.cmd7.Text = "7"
        Me.cmd7.UseVisualStyleBackColor = True
        '
        'cmd8
        '
        Me.cmd8.Location = New System.Drawing.Point(65, 150)
        Me.cmd8.Name = "cmd8"
        Me.cmd8.Size = New System.Drawing.Size(47, 45)
        Me.cmd8.TabIndex = 7
        Me.cmd8.Text = "8"
        Me.cmd8.UseVisualStyleBackColor = True
        '
        'cmd9
        '
        Me.cmd9.Location = New System.Drawing.Point(118, 150)
        Me.cmd9.Name = "cmd9"
        Me.cmd9.Size = New System.Drawing.Size(47, 45)
        Me.cmd9.TabIndex = 8
        Me.cmd9.Text = "9"
        Me.cmd9.UseVisualStyleBackColor = True
        '
        'cmdResta
        '
        Me.cmdResta.Location = New System.Drawing.Point(171, 99)
        Me.cmdResta.Name = "cmdResta"
        Me.cmdResta.Size = New System.Drawing.Size(47, 45)
        Me.cmdResta.TabIndex = 9
        Me.cmdResta.Text = "-"
        Me.cmdResta.UseVisualStyleBackColor = True
        '
        'cmdMultiplicacion
        '
        Me.cmdMultiplicacion.Location = New System.Drawing.Point(171, 150)
        Me.cmdMultiplicacion.Name = "cmdMultiplicacion"
        Me.cmdMultiplicacion.Size = New System.Drawing.Size(47, 45)
        Me.cmdMultiplicacion.TabIndex = 10
        Me.cmdMultiplicacion.Text = "*"
        Me.cmdMultiplicacion.UseVisualStyleBackColor = True
        '
        'cmdDivision
        '
        Me.cmdDivision.Location = New System.Drawing.Point(171, 201)
        Me.cmdDivision.Name = "cmdDivision"
        Me.cmdDivision.Size = New System.Drawing.Size(47, 45)
        Me.cmdDivision.TabIndex = 11
        Me.cmdDivision.Text = "/"
        Me.cmdDivision.UseVisualStyleBackColor = True
        '
        'cmdM
        '
        Me.cmdM.Location = New System.Drawing.Point(12, 201)
        Me.cmdM.Name = "cmdM"
        Me.cmdM.Size = New System.Drawing.Size(47, 45)
        Me.cmdM.TabIndex = 12
        Me.cmdM.Text = "M+"
        Me.cmdM.UseVisualStyleBackColor = True
        '
        'cmdMR
        '
        Me.cmdMR.Location = New System.Drawing.Point(118, 201)
        Me.cmdMR.Name = "cmdMR"
        Me.cmdMR.Size = New System.Drawing.Size(47, 45)
        Me.cmdMR.TabIndex = 13
        Me.cmdMR.Text = "MR"
        Me.cmdMR.UseVisualStyleBackColor = True
        '
        'cmdSuma
        '
        Me.cmdSuma.Location = New System.Drawing.Point(171, 48)
        Me.cmdSuma.Name = "cmdSuma"
        Me.cmdSuma.Size = New System.Drawing.Size(47, 45)
        Me.cmdSuma.TabIndex = 14
        Me.cmdSuma.Text = "+"
        Me.cmdSuma.UseVisualStyleBackColor = True
        '
        'lblResultado
        '
        Me.lblResultado.Location = New System.Drawing.Point(12, 9)
        Me.lblResultado.Name = "lblResultado"
        Me.lblResultado.Size = New System.Drawing.Size(206, 27)
        Me.lblResultado.TabIndex = 15
        Me.lblResultado.Text = "0"
        Me.lblResultado.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cmd0
        '
        Me.cmd0.Location = New System.Drawing.Point(65, 201)
        Me.cmd0.Name = "cmd0"
        Me.cmd0.Size = New System.Drawing.Size(47, 45)
        Me.cmd0.TabIndex = 16
        Me.cmd0.Text = "0"
        Me.cmd0.UseVisualStyleBackColor = True
        '
        'cmdIgual
        '
        Me.cmdIgual.Location = New System.Drawing.Point(12, 252)
        Me.cmdIgual.Name = "cmdIgual"
        Me.cmdIgual.Size = New System.Drawing.Size(153, 45)
        Me.cmdIgual.TabIndex = 17
        Me.cmdIgual.Text = "="
        Me.cmdIgual.UseVisualStyleBackColor = True
        '
        'cmdC
        '
        Me.cmdC.Location = New System.Drawing.Point(171, 252)
        Me.cmdC.Name = "cmdC"
        Me.cmdC.Size = New System.Drawing.Size(47, 45)
        Me.cmdC.TabIndex = 18
        Me.cmdC.Text = "C"
        Me.cmdC.UseVisualStyleBackColor = True
        '
        'frmCalculadora
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(226, 305)
        Me.Controls.Add(Me.cmdC)
        Me.Controls.Add(Me.cmdIgual)
        Me.Controls.Add(Me.cmd0)
        Me.Controls.Add(Me.lblResultado)
        Me.Controls.Add(Me.cmdSuma)
        Me.Controls.Add(Me.cmdMR)
        Me.Controls.Add(Me.cmdM)
        Me.Controls.Add(Me.cmdDivision)
        Me.Controls.Add(Me.cmdMultiplicacion)
        Me.Controls.Add(Me.cmdResta)
        Me.Controls.Add(Me.cmd9)
        Me.Controls.Add(Me.cmd8)
        Me.Controls.Add(Me.cmd7)
        Me.Controls.Add(Me.cmd6)
        Me.Controls.Add(Me.cmd5)
        Me.Controls.Add(Me.cmd4)
        Me.Controls.Add(Me.cmd3)
        Me.Controls.Add(Me.cmd2)
        Me.Controls.Add(Me.cmd1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Name = "frmCalculadora"
        Me.Text = "Calculadora"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents cmd1 As System.Windows.Forms.Button
    Friend WithEvents cmd2 As System.Windows.Forms.Button
    Friend WithEvents cmd3 As System.Windows.Forms.Button
    Friend WithEvents cmd4 As System.Windows.Forms.Button
    Friend WithEvents cmd5 As System.Windows.Forms.Button
    Friend WithEvents cmd6 As System.Windows.Forms.Button
    Friend WithEvents cmd7 As System.Windows.Forms.Button
    Friend WithEvents cmd8 As System.Windows.Forms.Button
    Friend WithEvents cmd9 As System.Windows.Forms.Button
    Friend WithEvents cmdResta As System.Windows.Forms.Button
    Friend WithEvents cmdMultiplicacion As System.Windows.Forms.Button
    Friend WithEvents cmdDivision As System.Windows.Forms.Button
    Friend WithEvents cmdM As System.Windows.Forms.Button
    Friend WithEvents cmdMR As System.Windows.Forms.Button
    Friend WithEvents cmdSuma As System.Windows.Forms.Button
    Friend WithEvents lblResultado As System.Windows.Forms.Label
    Friend WithEvents cmd0 As System.Windows.Forms.Button
    Friend WithEvents cmdIgual As System.Windows.Forms.Button
    Friend WithEvents cmdC As System.Windows.Forms.Button

End Class
